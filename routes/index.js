import express from 'express'
import Company from '../models/dews/Company.js'

const router = express.Router()

/* GET home page. */
router.get('/', async function(req, res, next) {
  let data = await Company.searchByName('TATA SARANA');

  res.json(data);
})

export default router
