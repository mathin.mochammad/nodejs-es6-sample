import express from 'express'
import Company from '../models/dews/Company.js'

const router = express.Router()

/* GET Company listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource')
})

router.get('/search/:keyword', async (req, res, next) => {
  let data = await Company.searchByName(req.params.keyword);

  res.json(data);
})

router.get('/show-10-data-test', async (req, res, next) => {
  let data = await Company.query().where('company_name', 'like', '%bca%').limit(10);

  res.json(data);
})

export default router
