import knex from '../../config/database.js'
const db = knex.dews;

const tableName = 'COMPANY'

const query = () => {
    return db.table(tableName)
}

const searchByName = (name) => {
    return query().select('*').where('company_name', 'like', `%${name}%`)
}

export default { searchByName, query }