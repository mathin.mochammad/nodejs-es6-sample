import fs from 'fs'
import path from 'path'
import knex from '../config/database.js'

const getModelFiles = dir => fs.readdirSync(dir)
    .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))
    .map(file => path.join(dir, file))

const dewsFiles = getModelFiles(__dirname + '/dews')

const dewsModels = dewsFiles.reduce((modelsObj, filename) => {
    const initModel = require(filename)
    const model = initModel(knex)

    if (model) modelsObj[model.name] = model

    return modelsObj

}, {})

export default dewsModels