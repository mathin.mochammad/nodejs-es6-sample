import dotenv from 'dotenv'
dotenv.config()

export default {
  dews: {
    client: process.env.DEWS_DB_DIALECT || 'mssql',
    connection: {
      host: process.env.DEWS_DB_HOST || 'localhost',
      database: process.env.DEWS_DB_DATABASE || 'DEWS',
      user: process.env.DEWS_DB_USER || 'sa',
      password: process.env.DEWS_DB_PASSWORD || '',
    }
  }
}
